from UnbxdException import UnbxdException
from UnbxdResult import UnbxdResult
import httplib2
import json
import urllib

class UnbxdService :

	def getSort(sortMap): 		
		try:
			sort=""
			for i in sortMap:
				sort+=i+" "
				if sortMap[i] == 1:
					sort+="asc"
				elif sortMap[i] == -1:
					sort+="desc"
				sort+=","
			if sort[-1:] == ",":
				sort = sort[:-1]
			return urllib.urlencode(sort)
		
		except Exception as e:
			raise UnbxdException(str(e))

	def getFilter(attributeFilters=None,rangeFilter=None):
		try:
			filter="(*:* AND ("
			
			if attributeFilters != None:
				print attributeFilters
				for i in attributeFilters:
					if filter != "(*:* AND (":
						filter+=" AND ("

					for j in attributeFilters[i]:
						filter= filter + i + ":\"" + j + "\" OR "

					if filter[-4:] == " OR ":
						filter = filter[:-4]
					filter+=")"

			if rangeFilter != None:	
				for i in rangeFilter :
					if filter!= "(*:* AND (":
						filter+=" AND ("
					
					for j in rangeFilter[i]:
						filter=filter + i + ":[" 
						
						if j.getFrom() != None:
							filter += j.getFrom()
						else:	 
							filter +="*"

						filter+=" TO "

						if j.getTo() != None:
							filter += j.getTo()
						else:	 
							filter +="*"
						
						filter.append("] OR ")
					
					if filter[-4:] == " OR ":
						filter = filter[:-4]

					filter+=")"
						
			filter+=")"
			return urllib.urlencode(filter)
		
		except Exception as e:
			print e
			raise UnbxdException(str(e))


	def getQueryParam(self, params):
		if (params.getRuleSet() == "browse") :
			return "?category-id=" + params.getCategoryId()
		elif params.getRuleSet() == "filter":
			return "?cond="+ self.getFilter(params.getFilter(),params.getRangeFilter())
		else:
			if params.getQuery() != None  and  params.getQuery() != "":
				return "?q=" +params.getQuery()
			else:
				return "?q=" + "*"

	def prepareUrl(self, params, address):
		try:
			url=""
			url+=address+"/"+params.getRuleSet()+self.getQueryParam(params)+"&start="
			#if params.getStart() != None:
			if hasattr(params, '_start'):
				url+= params.getStart() 
			else:
				url+="0"
			
			url+="&rows="
			
			if hasattr(params,'_limit'):
				url+=params.getLimit()
			else:
			 	url+="20"
			
			if hasattr(params, '_filter') and hasattr(params,'_rangeFilter'):
				url+="&filter="+self.getFilter(params.getFilter(), params.getRangeFilter())
			elif hasattr(params, '_filter'):
				#url+="&filter="+self.getFilter(params.getFilter())
				#url+="&filter="+params.getFilter()
				print params.getFilter()
			elif hasattr(params, '_rangefilter'):
				url+="&filter="+self.getFilter(params.getRangeFilter())
			
			if hasattr(params,'_sort'):
				url+="&sort="+self.getSort(params.getSort())
			
			url+="&wt=json"
			return url
		
		except Exception as e:
			print e
			raise UnbxdException(str(e))
		 
	
	def search(self, params, address, spellCheck):
		
		 try:
			url = self.prepareUrl(params, address)
			print url
			response, content = httplib2.Http().request(url)
			if (response['status'] != "200") :
				raise UnbxdException("Method failed: " + response['status'])
			
			result = json.loads(content)
			unbxdResult = UnbxdResult(result)
			print result
			if(spellCheck == True):
				unbxdResult.setSpellCheckQuery(params.getQuery())
			
			if((unbxdResult!=None) and not(spellCheck) and (unbxdResult.getTotalHits() == 0) and (unbxdResult.getSpellSuggestion() != None)):
				params.setQuery(unbxdResult.getSpellSuggestion())
				return self.search(params, address, "true")
			
			return unbxdResult
			
		 except Exception as e:
		 	print e
			raise UnbxdException(str(e))