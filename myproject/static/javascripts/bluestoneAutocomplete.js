
function sack(file) {
	this.xmlhttp = null;

	this.resetData = function() {
		this.method = "POST";
  		this.queryStringSeparator = "?";
		this.argumentSeparator = "&";
		this.URLString = "";
		this.encodeURIString = true;
  		this.execute = false;
  		this.element = null;
		this.elementObj = null;
		this.requestFile = file;
		this.vars = new Object();
		this.responseStatus = new Array(2);
  	};

	this.resetFunctions = function() {
  		this.onLoading = function() { };
  		this.onLoaded = function() { };
  		this.onInteractive = function() { };
  		this.onCompletion = function() { };
  		this.onError = function() { };
		this.onFail = function() { };
	};

	this.reset = function() {
		this.resetFunctions();
		this.resetData();
	};

	this.createAJAX = function() {
		try {
			this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e1) {
			try {
				this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				this.xmlhttp = null;
			}
		}

		if (! this.xmlhttp) {
			if (typeof XMLHttpRequest != "undefined") {
				this.xmlhttp = new XMLHttpRequest();
			} else {
				this.failed = true;
			}
		}
	};

	this.setVar = function(name, value){
		this.vars[name] = Array(value, false);
	};

	this.encVar = function(name, value, returnvars) {
		if (true == returnvars) {
			return Array(encodeURIComponent(name), encodeURIComponent(value));
		} else {
			this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
		}
	}

	this.processURLString = function(string, encode) {
		encoded = encodeURIComponent(this.argumentSeparator);
		regexp = new RegExp(this.argumentSeparator + "|" + encoded);
		varArray = string.split(regexp);
		for (i = 0; i < varArray.length; i++){
			urlVars = varArray[i].split("=");
			if (true == encode){
				this.encVar(urlVars[0], urlVars[1]);
			} else {
				this.setVar(urlVars[0], urlVars[1]);
			}
		}
	}

	this.createURLString = function(urlstring) {
		if (this.encodeURIString && this.URLString.length) {
			this.processURLString(this.URLString, true);
		}

		if (urlstring) {
			if (this.URLString.length) {
				this.URLString += this.argumentSeparator + urlstring;
			} else {
				this.URLString = urlstring;
			}
		}

		// prevents caching of URLString
		this.setVar("rndval", new Date().getTime());

		urlstringtemp = new Array();
		for (key in this.vars) {
			if (false == this.vars[key][1] && true == this.encodeURIString) {
				encoded = this.encVar(key, this.vars[key][0], true);
				delete this.vars[key];
				this.vars[encoded[0]] = Array(encoded[1], true);
				key = encoded[0];
			}

			urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
		}
		if (urlstring){
			this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
		} else {
			this.URLString += urlstringtemp.join(this.argumentSeparator);
		}
	}

	this.runResponse = function() {
		eval(this.response);
	}

	this.runAJAX = function(urlstring) {
		if (this.failed) {
			this.onFail();
		} else {
			this.createURLString(urlstring);
			if (this.element) {
				this.elementObj = document.getElementById(this.element);
			}
			if (this.xmlhttp) {
				var self = this;
				if (this.method == "GET") {
					totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
					this.xmlhttp.open(this.method, totalurlstring, true);
				} else {
					this.xmlhttp.open(this.method, this.requestFile, true);
					try {
						this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
					} catch (e) { }
				}

				this.xmlhttp.onreadystatechange = function() {
					switch (self.xmlhttp.readyState) {
						case 1:
							self.onLoading();
							break;
						case 2:
							self.onLoaded();
							break;
						case 3:
							self.onInteractive();
							break;
						case 4:
							self.response = self.xmlhttp.responseText;
							self.responseXML = self.xmlhttp.responseXML;
							self.responseStatus[0] = self.xmlhttp.status;
							self.responseStatus[1] = self.xmlhttp.statusText;

							if (self.execute) {
								self.runResponse();
							}

							if (self.elementObj) {
								elemNodeName = self.elementObj.nodeName;
								elemNodeName.toLowerCase();
								if (elemNodeName == "input"
								|| elemNodeName == "select"
								|| elemNodeName == "option"
								|| elemNodeName == "textarea") {
									self.elementObj.value = self.response;
								} else {
									self.elementObj.innerHTML = self.response;
								}
							}
							if (self.responseStatus[0] == "200") {
								self.onCompletion();
							} else {
								self.onError();
							}

							self.URLString = "";
							break;
					}
				};

				this.xmlhttp.send(this.URLString);
			}
		}
	};

	this.reset();
	this.createAJAX();
}


	var ajaxBox_offsetX = 0;
	var ajaxBox_offsetY = 0;
	var ajax_list_externalFile = 'http://bluestone-u1389939445606.search.unbxdapi.com/c41c3828460f645ea24c23f935c26864/autosuggest';	// Path to external file
	var minimumLettersBeforeLookup = 1;	// Number of letters entered before a lookup is performed.

	var ajax_list_objects = new Array();
	var ajax_list_cachedLists = new Array();
	var ajax_list_activeInput = false;
	var ajax_list_activeItem;
	var ajax_list_optionDivFirstItem = false;
	var ajax_list_currentLetters = new Array();
	var ajax_optionDiv = false;
	var ajax_optionDiv_iframe = false;
	
	var autoSelField = 0;
	var depSelChange = false;	
	
	var ajax_list_MSIE = false;
	if(navigator.userAgent.indexOf('MSIE')>=0 && navigator.userAgent.indexOf('Opera')<0)ajax_list_MSIE=true;

	var currentListIndex = 0;

	function ajax_getTopPos(inputObj) {
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null){
	  	returnValue += inputObj.offsetTop;
	  }
	  return returnValue;
	}	
	
	function ajax_list_cancelEvent() {
		return false;
	}

	function ajax_getLeftPos(inputObj) {
		
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null)returnValue += inputObj.offsetLeft;

	  return returnValue;
	}

	function ajax_option_setValue(e,inputObj) {
		
		if(!inputObj)inputObj=this;
		var tmpValue = inputObj.innerHTML;
		if(ajax_list_MSIE)tmpValue = inputObj.innerText;else tmpValue = inputObj.textContent;
		if(!tmpValue)tmpValue = inputObj.innerHTML;
		ajax_list_activeInput.value = tmpValue;
		if(document.getElementById(ajax_list_activeInput.name + '_hidden'))document.getElementById(ajax_list_activeInput.name + '_hidden').value = inputObj.id;
		$('#search-form').submit();
		/*
  		//var f1=setTimeout('ajax_list_activeInput.focus()',1);
  		//var f2=setTimeout('ajax_list_activeInput.value = ajax_list_activeInput.value',1);

		ajax_options_hide();
		*/
	}

	function ajax_options_hide()
	{
		if(ajax_optionDiv)ajax_optionDiv.style.display='none';
		if(ajax_optionDiv_iframe)ajax_optionDiv_iframe.style.display='none';
	}	
	
	var lastActiveItem;
	function ajax_options_rollOverActiveItem(item,fromKeyBoard)
	{
		if(lastActiveItem)lastActiveItem.className='optionDiv';
		//if(ajax_list_activeItem)ajax_list_activeItem.className='optionDiv';
		item.className='optionDivSelected';
		ajax_list_activeItem = item;
		lastActiveItem = item;

		if(fromKeyBoard){
			if(ajax_list_activeItem.offsetTop>ajax_optionDiv.offsetHeight){
				ajax_optionDiv.scrollTop = ajax_list_activeItem.offsetTop - ajax_optionDiv.offsetHeight + 
																		ajax_list_activeItem.offsetHeight + 2 ;
			}
			if(ajax_list_activeItem.offsetTop<ajax_optionDiv.scrollTop)
			{
				ajax_optionDiv.scrollTop = 0;
			}
		}
	}

	function ajax_option_list_buildList(letters,paramToExternalFile){


		ajax_optionDiv.innerHTML = '';
		ajax_list_activeItem = false;
		
		if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()].length<=1){
			ajax_options_hide();
			return;
		}

		ajax_list_optionDivFirstItem = false;
		var optionsAdded = false;
		
		var catLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length <3? 
				ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length:3;
		var brandLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length <3?
						ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length:3;
		var prodLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length <4?
						ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length:4;
		
		var catDif = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length-3;
		var brandDif = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length-3;
		var prodDif = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length-4;
		
		
		if(catDif < 0 && brandDif>0  ){	
			//catLength += catDif;		
			if (-catDif > brandDif ){
				catDif += brandDif;
				brandDif = 0;
				brandLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length;
			} else if (-catDif <= brandDif ){
				brandLength = brandLength - catDif;
				brandDif += catDif;	
				catDif = 0;					
			}		
		} else if (brandDif < 0 && catDif>0 ){			
			//brandLength += brandDif;		
			if (-brandDif > catDif ){
				brandDif += catDif;
				catDif = 0;
				catLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length;
			} else if (-brandDif <= catDif ){
				catLength = catLength - brandDif;
				catDif += brandDif;
				brandDif = 0;						
			}		
		}
		if(catDif < 0 && prodDif>0  ){	
			//catLength = catLength+catDif;		
			if (-catDif > prodDif ){
				catDif += prodDif;
				prodDif = 0;
				prodLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length;
			} else if (-catDif <= prodDif ){
				prodLength = prodLength - catDif;
				prodDif += catDif;
				catDif = 0;						
			}
		} else if (prodDif < 0 && catDif>0 ){	
			//prodLength += prodDif;
			if (-prodDif > catDif ){
				prodDif += catDif;
				catDif = 0;
				catLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length;
			} else if (-prodDif <= catDif ){
				catLength = catLength - prodDif;
				catDif += prodDif;
				prodDif = 0;					
			}
		}
		if(brandDif < 0 && prodDif>0  ){	
			//brandLength += brandDif;		
			if (-brandDif > prodDif ){
				brandDif += prodDif;
				prodDif = 0;
				prodLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length;
			} else if (-brandDif <= prodDif ){
				prodLength = prodLength - brandDif;
				brandDif = 0;
				prodDif += brandDif;				
			}
		} else if (prodDif < 0 && brandDif>0 ){	
			//prodLength += prodDif;		
			if (-prodDif > brandDif ){
				prodDif += brandDif;
				brandDif = 0;
				brandLength = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length;
			} else if (-prodDif <= brandDif ){
				brandLength = brandLength - prodDif;
				prodDif = 0;
				brandDif += prodDif;			
			}
		}
		
		if (ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length >0){
			
			var catDiv = document.createElement('DIV');
			catDiv.style.color = '#003366'
			catDiv.style.fontSize='11pt';
			catDiv.style.fontWeight='600';
			
			ajax_optionDiv.appendChild(catDiv);
			//catDiv.innerHTML = 'Product Name';
			catDiv.innerHTML = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][2];
			
			for(var no=0;no<catLength;no++){
				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0][no].length==0)continue;
				optionsAdded = true;
				var div = document.createElement('DIV');
				var items = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0][no].split(/###/gi);

				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][0][0].length==1 && ajax_list_activeInput.value == items[0]){
					ajax_options_hide();
					return;
				}
				//div.innerHTML += '<img width="40" height="40" border="0" src="'+this.autoSuggObj1.response.products[no].image_url+'">';
				div.innerHTML += items[items.length-1];
			//	div.innerHTML += '<span> ('+this.autoSuggObj1.response.products[no].price+')</span>';
				div.id = items[0];
				div.className='optionDiv';
				div.onmouseover = function(){ ajax_options_rollOverActiveItem(this,false) }
				div.onclick = ajax_option_setValue;
				if(!ajax_list_optionDivFirstItem)ajax_list_optionDivFirstItem = div;
				ajax_optionDiv.appendChild(div);
			}			
		}
		
		if (ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length >0){
			
			if(optionsAdded){			
				
				var lineDiv = document.createElement('DIV');
				lineDiv.style.backgroundColor = '#000000';
				ajax_optionDiv.appendChild(lineDiv);			
			}
			
				var brandDiv = document.createElement('DIV');
				brandDiv.style.color = '#003366'
				brandDiv.style.fontSize='11pt';
				brandDiv.style.fontWeight='600';
			
			ajax_optionDiv.appendChild(brandDiv);
			//brandDiv.innerHTML = 'Category';
			brandDiv.innerHTML = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][2];
			
			for(var no=0;no<brandLength;no++){
				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0][no].length==0)continue;
				optionsAdded = true;
				var div = document.createElement('DIV');
				var items = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0][no].split(/###/gi);

				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][1][0].length==1 && ajax_list_activeInput.value == items[0]){
					ajax_options_hide();
					return;
				}
			//	div.innerHTML += '<img width="40" height="40" border="0" src="'+this.autoSuggObj1.response.products[no].image_url+'">';
				div.innerHTML += items[items.length-1];
			//	div.innerHTML += '<span> (Rs.'+this.autoSuggObj1.response.products[no].actualprice+')</span>';
				div.id = items[0];
				div.className='optionDiv';
				div.onmouseover = function(){ ajax_options_rollOverActiveItem(this,false) }
				div.onclick = ajax_option_setValue;
				if(!ajax_list_optionDivFirstItem)ajax_list_optionDivFirstItem = div;
				ajax_optionDiv.appendChild(div);
			}			
		}
		if (ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length >0){
			
			if(optionsAdded){			
				
				var lineDiv = document.createElement('DIV');
				lineDiv.style.backgroundColor = '#000000';
				ajax_optionDiv.appendChild(lineDiv);			
			}
			
			var productDiv = document.createElement('DIV');
			productDiv.style.color = '#003366';
				productDiv.style.fontSize='11pt';
			productDiv.style.fontWeight='600';
			
			ajax_optionDiv.appendChild(productDiv);
			//productDiv.innerHTML = 'Sub Category';
			productDiv.innerHTML = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][2];
			
			for(var no=0;no<prodLength;no++){
				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0][no].length==0)continue;
				optionsAdded = true;
				var div = document.createElement('DIV');
				var items = ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0][no].split(/###/gi);

				if(ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()][2][0].length==1 && ajax_list_activeInput.value == items[0]){
					ajax_options_hide();
					return;
				}

				div.innerHTML = items[items.length-1];
				div.id = items[0];
				div.className='optionDiv';
				div.onmouseover = function(){ ajax_options_rollOverActiveItem(this,false) }
				div.onclick = ajax_option_setValue;
				document.onclick = ajax_options_hide;
				if(!ajax_list_optionDivFirstItem)ajax_list_optionDivFirstItem = div;
				ajax_optionDiv.appendChild(div);
			}			
		}		
		
		if(optionsAdded){
			ajax_optionDiv.style.display='block';
			if(ajax_optionDiv_iframe)ajax_optionDiv_iframe.style.display='';
			// highlight first term by default
			//ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
		}
	}	

	function ajax_option_resize(inputObj)
	{
		ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
		ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj) + ajaxBox_offsetX) + 'px';
		if(ajax_optionDiv_iframe){
			ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
			ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;
		}
	}

	function ajax_showOptions(inputObj,paramToExternalFile,e,sitename,key)
	{
		
	//	autoSelField = document.getElementById('depSelId').value;

		if(e.keyCode==13 || e.keyCode==9)return;
		if(!ajax_list_currentLetters[autoSelField])ajax_list_currentLetters[autoSelField] = new Array();
		if(!depSelChange){
			if(ajax_list_currentLetters[autoSelField][inputObj.name]==inputObj.value)return;
		}
		
		//if(ajax_list_currentLetters[autoSelField][inputObj.name]==inputObj.value)return;
		if(!ajax_list_cachedLists[paramToExternalFile])ajax_list_cachedLists[paramToExternalFile] = new Array();
		if(!ajax_list_cachedLists[paramToExternalFile][autoSelField])ajax_list_cachedLists[paramToExternalFile][autoSelField] = new Array();
		
		ajax_list_currentLetters[autoSelField][inputObj.name] = inputObj.value;
		if(!ajax_optionDiv){
			ajax_optionDiv = document.createElement('DIV');
			ajax_optionDiv.id = 'ajax_listOfOptions';
			document.body.appendChild(ajax_optionDiv);

			if(ajax_list_MSIE){
				ajax_optionDiv_iframe = document.createElement('IFRAME');
				ajax_optionDiv_iframe.border='0';
				ajax_optionDiv_iframe.style.width = ajax_optionDiv.clientWidth + 'px';
				ajax_optionDiv_iframe.style.height = ajax_optionDiv.clientHeight + 'px';
				ajax_optionDiv_iframe.id = 'ajax_listOfOptions_iframe';

				document.body.appendChild(ajax_optionDiv_iframe);
			}

			var allInputs = document.getElementsByTagName('INPUT');
			for(var no=0;no<allInputs.length;no++){
					if(!allInputs[no].onkeyup)allInputs[no].onfocus = ajax_options_hide;
			}
			var allSelects = document.getElementsByTagName('SELECT');
			for(var no=0;no<allSelects.length;no++){
				allSelects[no].onfocus = ajax_options_hide;
			}

			var oldonkeydown=document.body.onkeydown;
			if(typeof oldonkeydown!='function'){
				document.body.onkeydown=ajax_option_keyNavigation;
			}else{
				document.body.onkeydown=function(){
					oldonkeydown();
				ajax_option_keyNavigation() ;}
			}
			var oldonresize=document.body.onresize;
			if(typeof oldonresize!='function'){
				document.body.onresize=function() {ajax_option_resize(inputObj); };
			}else{
				document.body.onresize=function(){oldonresize();
				ajax_option_resize(inputObj) ;}
			}
		}

		if(inputObj.value.length<minimumLettersBeforeLookup){
			ajax_options_hide();
			return;
		}

		ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
		ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj) + ajaxBox_offsetX) + 'px';
		if(ajax_optionDiv_iframe){
			ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
			ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;
		}

		ajax_list_activeInput = inputObj;
		ajax_optionDiv.onselectstart =  ajax_list_cancelEvent;
		currentListIndex++;
		if(ajax_list_cachedLists[paramToExternalFile][autoSelField][inputObj.value.toLowerCase()]){
			ajax_option_list_buildList(inputObj.value,paramToExternalFile,currentListIndex);
		}else{
			var tmpIndex=currentListIndex/1;
			//ajax_optionDiv.innerHTML = '';
			var ajaxIndex = ajax_list_objects.length;
			ajax_list_objects[ajaxIndex] = new sack();
			
				
			ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,tmpIndex,sitename,key);
		}
	}

	function ajax_option_keyNavigation(e)
	{
		if(document.all)e = event;

		if(!ajax_optionDiv)return;
		if(ajax_optionDiv.style.display=='none')return;	

		if(e.keyCode==38){	// Up arrow
			if(!ajax_list_activeItem)return;
			if(ajax_list_activeItem && !ajax_list_activeItem.previousSibling)return;
			ajax_options_rollOverActiveItem(/*ajax_list_activeItem.previousSibling*/getPreviosSuggItem(),true);
		}
		
		if(e.keyCode==40){	// Down arrow
			if(!ajax_list_activeItem){
				ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
			}else{
				if(!ajax_list_activeItem.nextSibling)return;
				ajax_options_rollOverActiveItem(/*ajax_list_activeItem.nextSibling*/getNextSuggItem(),true);
			}
		}
		
		if(e.keyCode==13 || e.keyCode==9){	// Enter key or tab key

			if(ajax_list_activeItem && ajax_list_activeItem.className=='optionDivSelected')ajax_option_setValue(false,ajax_list_activeItem);
			if(e.keyCode==13)return false; else return true;
		}
		
		if(e.keyCode==27){	// Escape key
			ajax_options_hide();
		}
	}
	
	function getPreviosSuggItem(){
		var prevItem = ajax_list_activeItem.previousSibling;
		
		if(prevItem.className == 'optionDiv'){
			return prevItem;
		} else {
			ajax_list_activeItem = ajax_list_activeItem.previousSibling;
			return getPreviosSuggItem();			
		}
	}
	
	function getNextSuggItem(){
		var nextItem = ajax_list_activeItem.nextSibling;
		
		if(nextItem.className == 'optionDiv'){
			return nextItem;
		} else {
			ajax_list_activeItem = ajax_list_activeItem.nextSibling;
			return getNextSuggItem();			
		}
	}

	document.documentElement.onclick = $('#search-form').submit();

	function autoHideList(e)
	{
		if(document.all)e = event;

		if (e.target){			
			source = e.target;
		}else if (e.srcElement){			
			source = e.srcElement;
		}
			if (source.nodeType == 3) // defeat Safari bug
				source = source.parentNode;
		if(source.tagName.toLowerCase()!='input' && source.tagName.toLowerCase()!='textarea'){
			
			if(source.parentNode != null){
			if(source.parentNode.id == 'ajax_listOfOptions'){				
				ajax_options_hide();		
				//querySearch("",0);
				unbxdApi.getSearchResults($("#searchBoxId").val(),'search',paintHomePage,1,12);
				}
			}
		}
	}
	
function ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,whichIndex,sitename,key) {
		
		if(whichIndex!=currentListIndex)return;
		var letters = inputObj.value.replace(" ","+");
		
		var autoWords = letters.split("+");
		
		var autoArr1 = new Array();
		var autoArr2 = new Array();
		var autoArr3 = new Array();
		
		var autoUrl1 = '';
		var autoUrl2 = '';
		var autoUrl3 = '';
		ajax_list_externalFile  = "http://"+sitename +".search.unbxdapi.com/"+key+"/autosuggest";
		

			
			autoUrl1 = ajax_list_externalFile+ '?' + 'q=' + letters + '&hl=true&hl.q=' + letters +'&hl.simple.pre=<strong>&hl.simple.post=</strong>';
						
		
	/*		$.ajax({		 
				  url: autoUrl1,
				  dataType: "jsonp",
				  jsonp: 'json.wrf',
				  success: function(data){*/
	
		jQuery.ajax({
				url: autoUrl1,
				dataType: "jsonp",
				 jsonp: 'json.wrf',
				success:function(a1){
				
				autoSuggObj1 = a1; //eval("("+a1[2].responseText+")");
				
				var elements = new Array();
				
				elements[0] = new Array();
				elements[1] = new Array();
				elements[2] = new Array();
				
					
					for (count=0;count < autoSuggObj1.response.products.length ;count++) {
						autoArr1[autoArr1.length] = autoSuggObj1.highlighting[autoSuggObj1.response.products[count].uniqueId].autosuggest; //autoSuggObj1.response.products[count].autosuggest;
					}
										
					
					elements[0][2] = '';
					elements[1][2] = 'Products';
					elements[2][2] = '';
					
					
					
				
					elements[0][0] = '';//uniqueSuggestion(autoArr2, letters.length);
					elements[1][0] = '';//uniqueSuggestion(autoArr1, letters.length);
					elements[2][0] = uniqueSuggestion(autoArr1, letters.length);
				
				elements[0][1] = '';//autoSuggObj2.response.products.length;
				elements[1][1] = '';//autoSuggObj1.response.products.length;
				elements[2][1] = autoSuggObj1.response.products.length;
				
				
				ajax_list_cachedLists[paramToExternalFile][autoSelField][letters.toLowerCase()] = elements;
				ajax_option_list_buildList(letters,paramToExternalFile);
		
		
			}

			});
	}	

	function sortAutoSugg(a,b){
		
		return b[1] - a[1];
	}
	
	function uniqueSuggestion(arrayName, len) {

		var newArray=new Array();
		var arrLen = arrayName.length;
		
		labelSug:for(var i=0; i<arrLen;i++ ) {		
		
			for(var j=0; j<newArray.length;j++ ) {
				arrayName[i] = arrayName[i].toString().replace(/[?!\-%$#@*+=.]+/g,' ');
				if(newArray[j]==arrayName[i]){
					continue labelSug;
				}
			}
		
			if(arrayName[i].length>0){
				arrayName[i] = arrayName[i].toString().replace(/[?!\-%$#@*+=.]+/g,' ');
				newArray[newArray.length] = arrayName[i];
			}
			if (newArray.length == 10){
			
				return newArray;
			}
		}
		return newArray;
	}