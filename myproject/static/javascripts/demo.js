var siteUrl = 'http://demo-u1393483043451.search.unbxdapi.com';
var noRes = 15;

/*var facetAr = new Array();
facetAr[facetAr.length] = 'category_fq';
facetAr[facetAr.length] = 'brand_fq';
facetAr[facetAr.length] = 'price_fq';*/

//Object.keys browser compatibility code starts
	if (!Object.keys) Object.keys = function(o) {
		if (o !== Object(o))
			throw new TypeError('Object.keys called on a non-object');
		var k=[],p;
		for (p in o) if (Object.prototype.hasOwnProperty.call(o,p)) k.push(p);
		return k;
	};
	//Object.keys browser compatibility code ends

var unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);

var searchText = '';
//unbxdApi.addFacetFl('category_fq');
//unbxdApi.addFacetFl('color_fq');
//unbxdApi.addFacetFl('brand_fq');
//unbxdApi.addFacetFl('gender_fq');

$(document).ready(function(){	

	homePageProducts();	

	//$('.dropdown-toggle').dropdown();

	//unbxdApi.loadNavigationNode(callbackNavNode);

    $(document).click(function(e) {
	if(e.target.id !== 'ajax_listOfOptions'){
	    $('#ajax_listOfOptions').hide();
	}
    });

	$("#exampleSearch").keydown(function(e) { 
	    if(e.which == 13) {
	    	//var brStr = '<li><a href="./demoSearch_files/demoSearch.html"><i class="icon-home"></i></a>'+
	    	//		 '<span class="divider">/</span></li><li class="active">Products</li>';
		//$('#ajax_listOfOptions').hide();
	    	unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);
	        searchText = $("#exampleSearch").val();
	        //unbxdApi.getSearchResults(searchText,'search',paintHomePage,1,noRes);
	        //paintpersona(0);
		
	        //$('.breadcrumb').empty();
	    	//$('.breadcrumb').append(brStr);
	    	$('#breadBox').empty();
	    }
	});

	$('#floatmenubar').on('click', 'a', function(){
		var catId = $(this).attr('navid');
	    $('#exampleSearch').val('');searchText = $("#exampleSearch").val();
		/*brStr = '<li><a href="./demoSearch_files/demoSearch.html"><i class="icon-home"></i></a> <span class="divider">/</span></li>';

		var elements = $(this).parents('.dropdown-submenu');
		for(var i = elements.length - 1; i >= 0; i--){
			var key = $(elements[i]).children('a').html();
			if(key !== $(this).html()){
				brStr += '<li class="active">'+ key +' <span class="divider">/</span></li>';
			}
		}*/
		
		unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);
	    unbxdApi.getSearchResults(catId,'browse',paintHomePage,1,noRes);
	    $(this).parents('.cat1dropdown').addClass('hideDropdown');
	    /*brStr += '<li class="active">'+$(this).html() +'</li>';
	    $('.breadcrumb').empty();
	    $('.breadcrumb').append(brStr);*/
	});

    $('[name=category]').on('mouseenter', function(){ 
		$(this).children('.cat1dropdown').removeClass('hideDropdown');
    });

	$(window).scroll(function() {
		if($(window).scrollTop() == $(document).height() - $(window).height()) {

			if(!unbxdApi.isBrowse){
				unbxdApi.getSearchResultsScroll(searchText,'search',paintScrollPage);
			
				//unbxdApi.getSearchResultsScroll(searchText,'search',paintScrollPage);
			} else {
				unbxdApi.getSearchResultsScroll(unbxdApi.navId,'browse',paintScrollPage);
			}
		}
	});

	$('#load-more').click(function(e){
		e.preventDefault();
		
		if(searchText == undefined || searchText.length==0)
			searchText = $("#exampleSearch").val();
	    
		if(!unbxdApi.isBrowse){
			unbxdApi.getSearchResultsScroll($("#exampleSearch").val(),'search',paintScrollPage);
		} else {
			unbxdApi.getSearchResultsScroll(unbxdApi.navId,'browse',paintScrollPage);
		}
	});

	$(".left-nav").on('click','[name=ckFacetId]',function(data){
		if($(this).is(':checked')){
			//return;
			unbxdApi.addFq($(this).attr('fqfield'), $(this).attr('fqval'));
		} else {
			unbxdApi.removeFq($(this).attr('fqfield'), $(this).attr('fqval'));	
		}
	    
	    if(searchText == undefined || searchText.length==0) 
	    	searchText = $("#exampleSearch").val();
		
		if(!unbxdApi.isBrowse){
			var searchin = $("#exampleSearch").val().split(" in ");
			if(searchin[1] != null){
				unbxdApi.getautoSearchResults(searchin[0].trim(),searchin[1].trim(),'search',paintHomePage,1,15);
			}else{
				unbxdApi.getSearchResults(searchText,'search',paintHomePage,1,15);
			}
		//	unbxdApi.getSearchResults(searchText,'search',paintHomePage,1,noRes);
		} else {
			unbxdApi.getSearchResults(unbxdApi.navId,'browse',paintHomePage,1,noRes);
		}
	});

	$("#breadBox").on('click','.removenofloat',function(data){
	unbxdApi.removeFq($(this).find('.closeBtn').attr('fqfield'), $(this).find('.closeBtn').attr('fqval'));

	    if(searchText == undefined || searchText.length==0) searchText = $("#exampleSearch").val();

		if(!unbxdApi.isBrowse){
			unbxdApi.getSearchResults($("#exampleSearch").val(),'search',paintHomePage,1,noRes);
		} else {
			unbxdApi.getSearchResults(unbxdApi.navId,'browse',paintHomePage,1,noRes);
		}

		return false;
	});

	$("#breadBox").on('click','#clearAllTrigger',function(data){

		unbxdApi.clearFq();
		if(searchText == undefined || searchText.length==0) searchText = $("#exampleSearch").val();
		if(!unbxdApi.isBrowse){
			unbxdApi.getSearchResults($("#exampleSearch").val(),'search',paintHomePage,1,noRes);
		} else {
			unbxdApi.getSearchResults(unbxdApi.navId,'browse',paintHomePage,1,noRes);
		}

		return false;
	});

	unbxdApi.uuid = (decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent('unbxd.userId').replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")));

	unbxdApi.getRecentlyViewed(getUserId());
	unbxdApi.getRecommendations(getUserId());
});

function getUserId(){
	return (decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent('unbxd.userId').replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")));	
}

function getProducts(){	

	unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);

	if(searchText == undefined || searchText.length==0) searchText = $("#exampleSearch").val();

    unbxdApi.getSearchResults($("#exampleSearch").val(),'search',paintHomePage,1,noRes);
    var brStr = '<li><a href="./demoSearch_files/demoSearch.html"><i class="icon-home"></i></a> <span class="divider">/</span></li><li class="active">Products</li>';
    $('.breadcrumb').empty();
	$('.breadcrumb').append(brStr);
}

function homePageProducts(){	
	//unbxdApi.getGroupedResults('*','PrdTypeDesc','search',paintHomePage,1,10);
	unbxdApi.getSearchResults('*','search',paintHomePage,1,noRes);
}

function paintHomePage(response){
	//ajax_options_hide();
	//var respData = response.grouped.PrdTypeDesc;
	//paintProdGroupPage(respData);
	
 	/*$("#spellHolder").empty();
	if(response.hasOwnProperty('didYouMean')){
        if(response.numberOfProducts>10)
        {
       /* $("#spellHolder").html('<div class="row"><div class="span9">'
                +'<h2 class="title" style="font-size:15px; font-family:arial; font-weight: bold;">Did You Mean : '+spellWorld
                +'</h2><hr></div></div>');*/
     /*   paintProductPage(response);
        }
        else
        {
        searchText = spellWorld = response.didYouMean[0].suggestion;
        unbxdApi.getSearchResults(spellWorld,'search',paintProductPage,1,100);
        $("#spellHolder").html('<div class="row"><div class="span9">'
            +'<h2 class="title" style="font-size:15px; font-family:arial; font-weight: bold;">Did You Mean : '+spellWorld
            +'</h2><hr></div></div>');
        }
    }*/

	/*if(response.hasOwnProperty('didYouMean') ){
	   
		if(response.didYouMean.length > 0){

			searchText = spellWorld = response.didYouMean[0].suggestion;
			unbxdApi.getSearchResults(spellWorld,'search',paintHomePage,1,noRes);
			$("#spellHolder").html('<div class="row"><div class="span9">'
				+'<h2 class="title">Did You Mean : '+spellWorld
				+'</h2><hr></div></div>');
		}
	}*/
	//else{
		paintProductPage(response);
		paintFacet(response);
	//}
}

function paintRecommendationPage(response){}

function paintnavPage(id){
	var url="http://demolenskart-u1381944047658.search.unbxdapi.com/ae30782589df23780a9d98502388555f/browse?category-id="+id+"&wt=json";
    $('.dropdown-parent').style.visibility = hidden;
    
    $.ajax({
		url: url,
		type: "post",
		dataType: "jsonp",
		jsonp: 'json.wrf',	        
		success: paintProductPage        
	});

	//unbxdApi.getNavigation(url,paintProductPage);	
}

/*function paintProductPage(resp){
	//$("#spellHolder").empty();
	var respData = resp.response;

	var prodTile = '{{#products}}'
		/*+'<div id="{{uniqueId}}" class="modal hide fade" tabindex="-1">'
		+'<div class="modal-header">'
		+'<a class="close" data-dismiss="modal">x</a>'
		+'<h3>{{title}}</h3>'
		+'</div>'
		+'<div class="modal-body">'
		+'<img src="{{imageUrl}}">'
		+'<div>'
		+'<p>Unbxd Search</p>'
		+'</div>'		        
		+'</div>'
		+'<div class="modal-footer">'
		+'<a href="#" class="btn btn-success">Add to Cart</a>'
		+'<a href="#" class="btn" data-dismiss="modal">Close</a>'
		+'</div>'
		+'</div>'
		+'<li id="{{uniqueId}}" style="width:250px;height:350px;">'
   		+'<div class="product-view hidden-phone" id="product-view_73124">'
        +'<a href="#" title="Demo" class="product-image" style="margin-left:25px; display:block;" unbxdattr="product" unbxdparam_sku="{{uniqueId}}"  unbxdparam_prank="{{@index}}">'
      	+'<img id="productimgover73124" class="lazy" src="{{imageUrl}}" alt="{{description}}" style="width:180px;height:185px;"></a>'
      	+'<div class="name-view">{{title}}</div>'
      	+'<div class="brand-view">{{gender}}</div>'      
      	+'<div class="price-view"><i class="icon-cash">&nbsp;</i>Rs.{{price}}</div>'
      	+'<button type="button" class="AddToCart" unbxdattr="AddToCart" unbxdparam_sku="{{uniqueId}}">Add to Cart</button>'
   		+'</div>'   
		+'</li>{{/products}}';

	var template = Handlebars.compile(prodTile);	
	$("#prodListId").html(template(respData));

	facetSelEl = '<div style="color:#000; font:normal 13px Arial; margin:0; letter-spacing:1px; float:left">- '
		+resp.response.numberOfProducts+' products found </div>';
	
	//$("#breadBox").html(facetSelEl);

	// 369 Products
	/*$("#breadcrumbResultArea").html('<ul class="results"><li class="last"><strong>'
		+numberWithCommas(respData.numberOfProducts)
		+'</strong> Products</strong></li></ul>');*/

	// we found 369 results for â€œintimate clothesâ€�
	/*$("#searchMessagingHeader").html('<a name="mainBody"></a><h2><span class="search-message-header">'
		+'we found  <span> '+numberWithCommas(respData.numberOfProducts)+' </span>results for â€œ<span>'+
		+resp.searchMetaData.queryParams.q +'</span>â€� </span></h2>');

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}*/

Handlebars.registerHelper('imagePath', function(url) {
  return url.length == 1 ? url[0] : '/img/logo-UNBXD.png';
});

function productElement(result){
	this.result = result;

	this.result.index = $("#products").children().length + 1;

	var html = $(this.template(this.result))
		,self = this;

	html.click(function(e){
		e.preventDefault();

		productView(html,self.result);
	});

	$("#products").append(html);
}

productElement.prototype.template = Handlebars.compile('<li id="{{uniqueId}}" style="width:250px;height:262px;" unbxdattr="product" unbxdparam_sku="{{uniqueId}}"  unbxdparam_prank="{{index}}">'
	+'<div class="product-view hidden-phone" id="product-view_73124">'
	+'<a href="#" title="Demo" class="product-image" style="margin-left:25px; display:block;">'
	+'<img id="productimgover73124" class="lazy" src="{{imagePath imageUrl}}" alt="{{productDescription}}" style="width:180px;height:185px;"></a>'
	+'<div class="name-view">{{title}}</div>'
/*	+'<div class="brand-view">{{gender}}</div>'*/
/*	+'<div class="price-view"><i class="icon-cash">&nbsp;</i>Rs. {{price}}</div>'*/
	+'<button class=" btn-small btn">view details</button>'
	+'</div>'
	+'</li>');

function paintProductPage(resp){
	$("#products").html('');

	for(var i in resp.response.products){
		if(resp.response.products.hasOwnProperty(i))
			new productElement(resp.response.products[i]);
	}
}

var viewTemplate = Handlebars.compile(
	'<li class="main_view">'
	    +'<div class="main-grey-overlay"></div>'
	    +'<div class="top_arrow"></div>'
	    +'<div class="main-content">'
	        +'<div class="clearfix">'
	        	+'<div class="lt main-left-cont">'
	        		+'<img src="{{imagePath imageUrl}}" alt="{{productDescription}}" style="max-width:300px !important;height:300px;">'
	        	+'</div>'
	        	+'<div class="lt main-right-container">'
	                +'<a href="#" class="close-main-view">&times;</a>'
	                +'<h2>{{title}}</h2>'
	                +'<p>{{description}}</p>'
	                +'<h2>Rs. {{price}}</h2>'
	                +'<button type="button" class="AddToCart" unbxdattr="AddToCart" unbxdparam_sku="{{uniqueId}}">Add To Cart</button>'
	        	+'</div>'
	        +'</div>'
	        +'<div class="main-recos-title">You may also like</div>'
	        +'<div class="main-recos">'
	          /*+'<div class="indi-reco"></div>'
	          +'<div class="indi-reco"></div>'
	          +'<div class="indi-reco"></div>'
	          +'<div class="indi-reco"></div>'
	          +'<div class="indi-reco"></div>'*/
	        +'</div>'
	    +'</div>'
	+'</li>');

function productView(element,result){
	var temp = $(viewTemplate(result)), el = '', remainder = result.index % 3;
	temp.find('div.top_arrow').addClass('top_arrow_'+(remainder));

	if(remainder == 1)
		el = element.next().next();
	else if(remainder == 2)
		el = element.next();
	else
		el = element;

	$('li.main_view').remove();

	temp.find('.close-main-view').click(function(e){
		e.preventDefault();
		temp.remove();
	});

	temp.find('.AddToCart').click(function(e){
		e.preventDefault();
		cartCookies.setRUpdateCookie(result.uniqueId, {
			title: result.title
			,uniqueId: result.uniqueId
			,imageUrl : result.imageUrl
			,price: result.price
		});
	});

	temp.hide();
	temp.insertAfter(el);
	temp.slideDown();

	//reco api
	var uidCookie = (decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent('unbxd.userId').replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")));
	
	//unbxdApi.getAlsoViewed(uidCookie,paintAlsoViewPage);
	unbxdApi.getMoreLikeThese(result.uniqueId,paintAlsoViewPage);
	var wh = $(window).height(), st = $(window).scrollTop(), top = (temp.position()).top, height = temp.height();

	if((wh+st) < (top+height))
	$(window).scrollTop(st + (top+height - wh - st));

	unbxdApi.getRecentlyViewed(getUserId());
}

function paintAlsoViewPage(response)
{
	/*console.log("in paint Also Viewed");
	var respData = response.Recommendations; */
	
    var c = $('.main-recos');
    c.html('');

    if(response.count == 0){
    	$('.main-recos-title').remove();
    	return;
    }

    for(var i in response.Recommendations){
    	if(response.Recommendations.hasOwnProperty(i)){
    		var t = '<div class="indi-reco" unbxdattr="product" unbxdparam_sku="'+ response.Recommendations[i].uniqueId +'">'
    			+'<img src="'+(response.Recommendations[i].imageUrl ? response.Recommendations[i].imageUrl : '/img/logo-UNBXD.png')+'" style="height: 100px;max-width: 100px !important" data-uniqueId="'+ response.Recommendations[i].uniqueId +'"/>'
    			+'<div class="indi-reco-title">'+response.Recommendations[i].title+'</div>'
    		+'</div>'
    		,o = {
    			imageUrl : response.Recommendations[i].imageUrl
    			,description : response.Recommendations[i].description
    			,title : response.Recommendations[i].title
    			,price : response.Recommendations[i].price
    			,uniqueId : response.Recommendations[i].uniqueId
    		}
    		,f = (function(resp){return function(){
    			$('.main-grey-overlay').show();
    			var temp = $(viewTemplate(resp))
    			,html = temp.find('.main-content');

    			html

				$('li.main_view > .main-content').html(html.html());

				$('li.main_view .close-main-view').click(function(e){
					e.preventDefault();
					$('li.main_view').remove();
				});

				$('li.main_view .AddToCart').click(function(e){
					e.preventDefault();
					cartCookies.setRUpdateCookie(resp.uniqueId, {
						title: resp.title
						,uniqueId: resp.uniqueId
						,imageUrl : resp.imageUrl
						,price: resp.price
					});
				});

    			$('.main-grey-overlay').hide();
				unbxdApi.getMoreLikeThese(resp.uniqueId,paintAlsoViewPage);
				unbxdApi.getRecentlyViewed(getUserId());
    		};})(o);

    		c.append(t);
    		c.children().last().click(f);
    	}
    }
}

function paintScrollPage(resp){
	for(var i in resp.response.products){
		if(resp.response.products.hasOwnProperty(i)){
			new productElement(resp.response.products[i]);
		}
	}
}

/*function paintProdGroupPage(respData){

	var catTitle = '{{#groups}}<div class="row"><div class="span9"><h2 class="title">+ {{groupValue}}</h2><hr></div></div>';

	var a1 = '<div class="row"><div class="span9">'+
	  		'<ul class="thumbnails listing-products">{{#doclist.products}}'+
	  			'<li class="span3">'+
	  				'<div class="product-box">'+
	  					'<a href="http://wbpreview.com/previews/WB0M3G9S1/product_detail.html"><h4 style="height:40px;">{{PROD_NAME}}</h4></a>'+
	  					'<a  href="http://wbpreview.com/previews/WB0M3G9S1/product_detail.html">'+
	  					'<img style="height: 120px; margin-top: 20px;" src="{{image_url}}" alt=""></a>'+
	  					'<p>Category: <strong>{{Category}}</strong></p>'+
	  					'<div class="bottom">'+
	  						'<a href="http://wbpreview.com/previews/WB0M3G9S1/product_detail.html" class="view">view</a> / <a href="http://wbpreview.com/previews/WB0M3G9S1/cart.html" class="addcart">add to cart</a>'+
	  					'</div>'+
	  				'</div>'+
	  			'</li>{{/doclist.products}}'+
	  		'</ul>'+
	'</div></div>';

	catTitle += a1+'{{/groups}}';

	var template = Handlebars.compile(catTitle);	

	$("#productsListHolder").html(template(respData));
}*/


var fieldDisplayMap = new Array();
fieldDisplayMap.push({"key": "Category_fq", "value":"Category"});
fieldDisplayMap.push({"key":"gender_fq", "value":"gender"});

/*
  This function gets the mapped display values of the facet field.
*/
function getDisplayFieldValue(fieldName){
	for(var count=0; count<fieldDisplayMap.length; count++){
		if ( fieldDisplayMap[count].key == fieldName) 
			return fieldDisplayMap[count].value;
	}

	return fieldName;
}

function paintFacet(resp){

	facetData = resp.facets;

	var str1='<div class="divli" style="margin:5px 0px 10px 0; width:184px;">'
		,facetKeyAr = Object.keys(facetData)
		,facetEl = ''
		,facetSelEl = '';

	for ( var count =0; count<facetKeyAr.length ; count++) {
		var facetObj = facetData[facetKeyAr[count]]
			,dispField = getDisplayFieldValue(facetKeyAr[count])
			//mark facet selected if already selected
			,facetSel = unbxdApi.isFacetSelected(facetKeyAr[count]);

		if(facetSel){
			for(var facCount=0;facCount<facetSel.fieldVal.length;facCount++){
				facetSelEl += '<li class="removenofloat" style="position:relative;">'
					+'<a style="display:block;color:#585858 !important;" relmselect="men" href="#">'
					+facetSel.fieldVal[facCount]+'&nbsp;'
					+'<img src="./your_selection_icon.png" class="closeBtn" '
					+ 'fqField="'+facetKeyAr[count]+'"  fqVal="'+facetSel.fieldVal[facCount]+'" '
					+' style="position:absolute; top:0px;"></a>'
					+'</li>';
			}
			//continue;
		} 
 
		if(facetObj.values.length >0){
			facetEl += str1;
			facetEl += '<h3 class="left-nav-head">'
				+trimfq(dispField)+'<span class="forspan" style="display:none;">'+dispField+'</span></h3>';

			if(facetObj.type == 'facet_fields'){
				facetEl += '<div class="NcatSelul tinyscrl">'
	      			+'<div class="facetblock">'
	      			+'<ul style="color: #626262;font-size: 12px;font-family: arial;padding-bottom: 5px;line-height: 12px;">';

				for(var count1 =0; count1<(facetObj.values.length/2); count1++ ){
					facetEl += 	'<li style="list-style-type: none; margin-left: -31px;""><input type="checkbox" name="ckFacetId" id="'+facetObj.values[count1*2]+'_chk"  '
						+'unbxdParam_facetName="'+facetKeyAr[count]+'" unbxdParam_facetValue="'
						+facetObj.values[count1*2]+'"  class="fqFieldCl" fqField="'+facetKeyAr[count]
						+'" fqVal="'+facetObj.values[count1*2]+'" '
						+facetChecked(facetSel,facetObj.values[count1*2])+' >'
						+'<label style="display: inline-block;vertical-align: top;" for="'+facetObj.values[count1*2]+'_chk">'
						+'<span class="dimensionlabel">&nbsp;'+facetObj.values[count1*2]
							+' ('+facetObj.values[count1*2+1]+') </span>'
						+'</label></li>';
				}

				facetEl += '</ul></div></div>';
			}

			facetEl += '</div>';
		}	
	}

	$(".left-nav").html(facetEl);

	//$("#breadBox").empty();

	if(facetSelEl.length > 0)
		facetSelEl = '<div class="facetSelList" style="margin-left:10%; text-align:center;">'+facetSelEl+'</ul>';		

	facetSelEl += '<div style="color:#000; font:normal 15px Arial; font-weight:bold;">- '
		+resp.response.numberOfProducts+' PRODUCTS FOUND</div>';
	
	$("#breadBox").html(facetSelEl);

	function facetChecked(facetSel, fieldVal){

		if(! (facetSel && facetSel.fieldVal) ) return '';

		for(var count2=0;count2<facetSel.fieldVal.length;count2++){
			if(facetSel.fieldVal[count2] == fieldVal) return 'checked';
		}

		return '';
	}

}

function callbackNavNode(data){
	paintNavTree(unbxdApi.getNavTree(data));
}

function getNavChildren(obj, arr, status){
	//console.log(obj);
	var retStr = ''
		,childArr = arr
		,childKey = -1
		,parentKey = -1;
	
	parentKey = unbxdApi.filtered.indexOf(obj.id.toString());

	if(obj.hasOwnProperty('children')){
		if(parentKey !== -1)
			retStr = '<ul class="dropdown-menu">';
		
		for(var count1 = 0; count1 < obj.children.length; count1++){
			var recStr = '';
			parentKey = unbxdApi.filtered.indexOf(obj.children[count1].parent);
			childKey = unbxdApi.filtered.indexOf(obj.children[count1].id.toString());
			if( parentKey !== -1 ){
				if(status){
					recStr = getNavChildren(obj.children[count1], childArr, true);
					if(childKey !== -1){
						retStr += '<li class="dropdown-submenu">';	
					} else {
						retStr += '<li>';
					}
				} else {
					retStr += '<li>';
				}
				retStr += '<a tabindex="-1" href="#" navId="'+obj.children[count1].id+'">'+obj.children[count1].name+'</a>'
					+ recStr + '</li>';
			} else {
				retStr += '<li><a tabindex="-1" href="#" navId="'+obj.children[count1].id+'">'+obj.children[count1].name+'</a></li>';
			}
		}
		if(parentKey !== -1)
			retStr += '</ul>';
	} else {
		parentKey = unbxdApi.filtered.indexOf(obj.id.toString());
		// if(parentKey === -1){
		// 	console.log(parentKey);
		// 	console.log(obj);
		// }
		if(parentKey !== -1){
			retStr += getNavChildren(childArr[parentKey], childArr, true);
		} else if(parentKey === -1){
			childKey = unbxdApi.nodes.indexOf(obj.id);
			retStr += '<ul class="dropdown-menu">'
				+ '<li><a tabindex="-1" href="#" navId="'+unbxdApi.tree[childKey].id+'">'+unbxdApi.tree[childKey].name+'</a></li>'
				+ '</ul>';
		}
	}
	
	return retStr;
}

function paintNavTree(navTree) {
	/*var getNavTreeNode = function(parentNode){
		return parentNode.parent;
	}*/
	
	var navStr = '<ul class="dropdown-menu" id="menu1" role="menu">';

	for(var count = 0; count < navTree.length; count++){
		var childStr = '';
		if(navTree[count].id !== 1 && navTree[count].parent === "0"){
			if(navTree[count].hasOwnProperty('children')){
				childStr = getNavChildren(navTree[count], navTree, true);
				navStr += '<li class="dropdown-submenu"><a tabindex="-1" href="#" navId="'+navTree[count].id+'">'
				+ navTree[count].name + '</a>'
				+ childStr + '</li>';
			} else {
				navStr += '<li><a tabindex="-1" href="#" navId="'+navTree[count].id+'">'+navTree[count].name+'</a></li>'
			}
		}
	}
	navStr += '</ul>';
	$('#dropdown-navigate').append(navStr);
}

function browseResults(browseid){
	unbxdApi.getSearchResults(browseid,'browse',paintHomePage,1,noRes);
}

function clearCheckbox(){
	unbxdApi.clearFq();

	$('.fqFieldCl').attr("checked", false);

	unbxdApi.getSearchResults($("#exampleSearch").val(),'search',paintHomePage,1,15);
	
}

function landingpageresults(query){
	unbxdApi.getSearchResults(query,'search',paintHomePage,1,noRes);
}

var personnaId=1;
function paintclickpage(query){
	unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);
	unbxdApi.setPersonnaId(personnaId);
	searchText = query;
    $('#exampleSearch').val(query);
	unbxdApi.getSearchResults(query,'search',paintHomePage,1,noRes);
}

function filterPrice(min_amount,max_amount)
{
	unbxdApi.addFacetFl('price_fq');

	var url="http://demolenskart-u1381944047658.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q=*"+"&filter=price:["+min_amount+" TO "+max_amount+"]";

	$.ajax({
		url: url,
		type: "post",
		dataType: "jsonp",
		jsonp: 'json.wrf',
		success: paintProductPage
	});
}



function paintpersona(id) {

	unbxdApi = new UnbxdApi('ae30782589df23780a9d98502388555f', siteUrl);

	if(id>0) personnaId=id;
	else id=personnaId;

	//unbxdApi.setPersonnaId(id);



    var queryVal = (($("#exampleSearch").val() === '')? '*':$("#exampleSearch").val());
    //console.log(queryVal);

    unbxdApi.getSearchResults(queryVal,'search',paintHomePage,1,noRes);


	/*if(id==0)
    {
	    url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal;

	$.ajax({
	        url: url,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: paintProductPage        
	    });

	}
	if(id==1)
	{
			url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal
			+"&boost=if(comparerange(price,200,300,'equal to'),4,1)&boost=if(comparerange(price,300,400,'equal to'),6,1)"
			+"&boost=if(comparerange(price,400,600,'equal to'),9,1)";



	$.ajax({
	        url: url,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: paintProductPage        
	    });

	}
	if(id==2)
	{
		//url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal+"&start=0&rows=12&wt=json&fq={!tag=category_fq}(category_fq:%22Casual%20Shirts%22%20OR%20category_fq:%22Casual%22)&fq={!tag=gender_fq}(gender_fq:%22men%22)&facet.field={!ex=category_fq}category_fq&facet.field=colour_fq&facet.field=brand_fq&facet.field={!ex=gender_fq}gender_fq";

		url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal+"&start=0&rows=12&wt=json&fl=category,name,gender,brand,price,image_url,score&boost=if(compare('gender_fq','men','equal%20to'),4,1)&boost=if(compare('category_fq','Sneakers','equal%20to'),2.5,1)&boost=if(or(compare('category_fq','Dress%20Shoes','equal%20to'),compare('category_fq','Boots','equal%20to')),.25,1)";

		$.ajax({
	        url: url,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: paintProductPage        
	    });

	}
	if(id==3)
	{
		url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal+"&start=0&rows=12&wt=json&fq={!tag=category_fq}(category_fq:%22Dress%20Shirts%22%20OR%20category_fq:%22Dress%20Shoes%22)&fq={!tag=gender_fq}(gender_fq:%22men%22)&facet.field={!ex=category_fq}category_fq&facet.field=colour_fq&facet.field=brand_fq&facet.field={!ex=gender_fq}gender_fq";

		$.ajax({
	        url: url,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: paintProductPage        
	    });

	}
	if(id==4)
	{
		url="http://recdemo-u1387437363464.search.unbxdapi.com/ae30782589df23780a9d98502388555f/search?q="+queryVal+"&start=0&rows=12&wt=json&fq={!tag=category_fq}(category_fq:%22Pumps%22%20OR%20category_fq:%22Shirts%22)&fq={!tag=gender_fq}(gender_fq:%22women%22)&facet.field={!ex=category_fq}category_fq&facet.field=colour_fq&facet.field=brand_fq&facet.field={!ex=gender_fq}gender_fq";

		$.ajax({
	        url: url,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: paintProductPage        
	    });
	}*/

}

function trimfq(txt){
	return txt.replace("_fq","");
}
