class UnbxdException:
	
	_NORESPONSE = "Couldn't connect to Unbxd";
	_NOSITENAME = "Site name cannot be null";
	_NOAPIKEY = "Api key cannot be null";
	
	def getNORESPONSE(self):
		return self._NORESPONSE;
	

	def setNORESPONSE(self, nORESPONSE):
		self._NORESPONSE = nORESPONSE;
	

	def getNOSITENAME(self):
		return self._NOSITENAME;
	

	def setNOSITENAME( self,nOSITENAME):
		self._NOSITENAME = nOSITENAME;
	

	def getNOAPIKEY(self):
		return self._NOAPIKEY;
	

	def setNOAPIKEY( self,nOAPIKEY):
		self._NOAPIKEY = nOAPIKEY;

	def __init__(self, errorMessage, errorCode= None):
		self._errorMessage = errorMessage

		if errorCode != None:
			self._errorCode = errorCode