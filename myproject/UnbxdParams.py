class UnbxdParams:

    def getTimeout(self):
        return self._timeout

    def setTimeout(self,timeout):
        self._timeout = timeout

    def getSort(self):
        return self._sort

    def setSort(self,field,value = None):
        if value == None:
            self._sort = field
            return
        self._sort.update(field, value)

    def getFilter(self):
        return self._filter

    def setFilter(self,field, value ):
        #adding ONE facet with multiple values

        if not(hasattr(self,"_filter")):
            self._filter={}
        if field in self._filter:
            self._filter.extend(value)
        else:
            self._filter[field]=value

    def getRangeFilter(self):
        return self._rangeFilter

    def setRangeFilter(self,field, value = None):
        
        if not(hasattr(self,"_setRangefilter")):
            self._setRangefilter={}
        if field in self._setRangefilter:
            self._setRangefilter.extend(value)
        else:
            self._setRangefilter[field]=value
        
    def getRuleSet(self):
        return self._ruleSet

    def setRuleSet(self,ruleSet):
        self._ruleSet = ruleSet

    def getQuery(self):
        return self._query

    def setQuery(self,query):
        self._query = query

    def getCategoryId(self):
        return self._categoryId

    def setCategoryId(self,categoryId):
        self._categoryId = categoryId

    def getStart(self):
        if self._start != None:
            return self._start
        else:
            return None

    def setStart( self,start):
        self._start = start

    def getLimit(self):
        return self._limit

    def setLimit(self, limit):
        self._limit = limit

    def isDebug(self):
        return self._debug

    def setDebug( self, debug):
        self._debug = debug
