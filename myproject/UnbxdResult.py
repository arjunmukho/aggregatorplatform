from UnbxdException import UnbxdException
class UnbxdResult:
	def getResult(self):
		return self._result

	def setResult(self, result):
		self._result = result	

	def getSpellCheckQuery(self):
		return self._spellCheckQuery
	

	def setSpellCheckQuery(self,spellCheckQuery):
		self._spellCheckQuery = spellCheckQuery
	

	def getResponse(self):
		return self._response
	

	def setResponse(self,response):
		self._response = response
	

	def getTook(self):
		return self._took
	

	def getTotalHits(self):
		return self._totalHits
	

	def setTook(self, took):
		self._took = took


	def setTotalHits(self,totalHits):
		self._totalHits = totalHits
	
	def __init__(self,response):
		try:
			self._response = response
			if not(self._response == None):
			
				self._totalHits = response["response"]["numberOfProducts"]
				if "queryTime" in response["searchMetaData"]:
					self._took = response["searchMetaData"]["queryTime"]
				else:
					self._took=0				
		
		except Exception as e:
			raise UnbxdException(e)
	
	
	def getProducts(self):
		try:
			if "products" in self._response["response"]:
				return self._response["response"]["products"]
			return None
		except Exception as e:
			raise UnbxdException(e)
	
	
	def hasFacets(self):
		try:
			if "facets" in  self._response:
				return True
			return False
		except Exception as e:
			raise UnbxdException(e)
	
	
	def getFacets(self):
		try:
			if "facets" in  self._response:
				return self._response["facets"]
			return None
		except Exception as e:
			raise UnbxdException(e)
	
	
	def getSpellSuggestion(self):
		try:
			if "didYouMean" in self._response:
				return self._response["didYouMean"][0]["suggestion"]
			return None
		except Exception as e:
			raise UnbxdException(e)
	
	def getQuery(self):
		try:
			if "queryParams" in self._response["searchMetaData"]:
				if "q" in self._response["searchMetaData"]["queryParams"]:
					return self._response["searchMetaData"]["queryParams"]["q"]
			return ""
		except Exception as e:
			raise UnbxdException(e)
	
	def getRows(self):
		try:
			if "queryParams" in self._response["searchMetaData"]:
				if "rows" in self._response["searchMetaData"]["queryParams"]:
					return self._response["searchMetaData"]["queryParams"]["rows"]
			return 0
		except Exception as e:
			raise UnbxdException(e)
	
	def getStart(self):
		try:
			if "queryParams" in self._response["searchMetaData"]:
				if "start" in self._response["searchMetaData"]["queryParams"]:
					return self._response["searchMetaData"]["queryParams"]["start"]
			return 0
		except Exception as e:
			raise UnbxdException(e)
				
	def getStats(self):
		try:
			if "stats" in self._response:
				return self._response.getJSONObject("stats")
			return None
		except Exception as e:
			raise UnbxdException(e)